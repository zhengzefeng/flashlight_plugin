#import "FlashlightPlugin.h"
#import <AVFoundation/AVFoundation.h>
@implementation FlashlightPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"flashlight_plugin"
            binaryMessenger:[registrar messenger]];
  FlashlightPlugin* instance = [[FlashlightPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

//- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
//  if ([@"getPlatformVersion" isEqualToString:call.method]) {
//    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
//  } else {
//    result(FlutterMethodNotImplemented);
//  }
//}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    NSLog (@"*********************************string is :%@", call.method);
  if ([@"getPlatformVersion" isEqualToString:call.method]) {
    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
  } else if ([@"turnOn" isEqualToString:call.method]) {
//     NSNumber *intensity = call.arguments[@"intensity"];
     [self turnOnWithIntensity:0.5];
     result(nil);
  } else if ([@"turnOff" isEqualToString:call.method]) {
        [self turnOff];
        result(nil);
     }
    else if ([@"hasLamp" isEqualToString:call.method]) {
        result([NSNumber numberWithBool:[self hasLamp]]);
    }
   else {
     result(FlutterMethodNotImplemented);
   }
}
- (bool) hasLamp
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    return ([device hasTorch] && [device hasFlash]);
}

- (void) turnOff
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch] && [device hasFlash]){
        [device lockForConfiguration:nil];
        [device setTorchMode:AVCaptureTorchModeOff];
        [device unlockForConfiguration];
    }
}

- (void)turnOnWithIntensity:(float)level
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch] && [device hasFlash]){
        [device lockForConfiguration:nil];
        NSError *error = nil;
        float acceptedLevel = (level < AVCaptureMaxAvailableTorchLevel ? level : AVCaptureMaxAvailableTorchLevel);
        NSLog(@"FLash level: %f", acceptedLevel);
        [device setTorchModeOnWithLevel:acceptedLevel error:&error];
        [device unlockForConfiguration];
    }
}

/*private func hasFlashlight () -> Bool{
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return false}
        return device.hasTorch
    }

    private func toggleFlash(on: Bool) {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }

        do {
            try device.lockForConfiguration()
            if (on && device.torchMode == AVCaptureDevice.TorchMode.off) {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            } else if (!on && device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            }

            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }*/
@end
