import 'dart:async';

import 'package:flutter/services.dart';

class FlashlightPlugin {
  static const MethodChannel _channel =
      const MethodChannel('flashlight_plugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> get turnOn async {
    await _channel.invokeMethod('turnOn');
    return "";
  }

  static Future<String> get turnOff async {
    await _channel.invokeMethod('turnOff');
    return "";
  }
}
